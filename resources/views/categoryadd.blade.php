@extends('layouts.template')

@section('content')

<div class="col-md-8">
	<h1 class="text-center">Enter A New Name</h1>
	<form class="px-5" method="POST">
		@csrf
	<div class="form-group">
	<input type="text" name="name" value="name" class="form-control">
</div>
<div class="form-group">
	<a href="" class="btn btn-outline-success" name="save">Save</a>
</div>

</form>

</div>

@endsection('content')